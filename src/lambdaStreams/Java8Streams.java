package lambdaStreams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Java8Streams {

	public static void main(String[] args) {
		List<Integer> lista = Arrays.asList(1,5,8,9,4,4,7,6,6);
		
		// JAVA 8
		
		/*
		 * Stream<Integer> map = lista.stream() .limit(3) .map(e -> e * 2);
		 * map.forEach(e -> System.out.println(e));
		 */
		
		long count = lista.stream()
				//.limit(2)
				//.filter(e -> e % 2 == 0)
				.count();
		System.out.println("count " + count);
		
		Optional<Integer> min = lista.stream()
			//.filter(e -> e % 2 == 0)
				.min(Comparator.naturalOrder());
		System.out.println("minimo " + min.get());
		
		
		List<Integer> novaLista= lista.stream()
				.filter(e -> e % 2 == 0)
					.collect(Collectors.toList());
			System.out.println("collect " + novaLista);

	}

}
