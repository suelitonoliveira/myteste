package lambdaStreams;

import java.util.Arrays;
import java.util.List;



public class Java8LambdaStream {

	public static void main(String[] args) {
		// JAVA 8: fun��es lambda
		//API de Stream
		// Stream - fluxo de dados
		
		//
		
		
		/* java 5
		 * for (Integer integer : asList) { System.out.println(integer); }
		 */
		
		List<Integer> asList = Arrays.asList(1,2,3,4,5,8,9,1,7,6,6,9,9);
		asList.stream()
		.skip(2) // ignora os 2 primeiros dados
		.limit(2)
					.filter(e -> e % 2 == 0) 
					.forEach(e -> System.out.println(e));
	

	}

}
