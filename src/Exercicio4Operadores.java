import java.util.Scanner;

public class Exercicio4Operadores {
	
	static final Integer IDADE_MINIMA_PARA_APOSENTAR = 55;
	
	static final Integer TEMPO_MINIMO_DE_CONTRIBUICAO = 25;

	public static void main(String[] args) {
		
		
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Idade: ");
		int idade = sc.nextInt();
		System.out.println("Tempo Contribui��o: ");
		int contribuicao = sc.nextInt();
		
		if (idade >= IDADE_MINIMA_PARA_APOSENTAR && contribuicao >= TEMPO_MINIMO_DE_CONTRIBUICAO) {
			System.out.println("Voc� pode se aposentar!");
		}else {
			System.out.println("Voc� ainda n�o pode se aposentar!");
		}
		
		
		sc.close();

	}

}
