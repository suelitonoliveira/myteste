import java.util.Scanner;

public class ExercicoIF01 {

	static final Double NOTA_MINIMA_PARA_PASSAR = 150.0;
	static final Double NOTA_MINIMA_MATERIAS = 60.0;
	
	/*
	 * static final Double NOTA_DESCLASSIFICATORIA_GERAL = 150.0;
	 * 
	 * static final Double NOTA_MINIMA = 60.0;
	 */
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Nota de Corte e 150 pontos");
		System.out.print("Digite a nota de Matematica: ");
		Integer nota1 = scanner.nextInt();

		System.out.print("Digite a nota de Portugues: ");
		Integer nota2 = scanner.nextInt();

		Integer total = nota1 + nota2;

		if (nota1 >= NOTA_MINIMA_MATERIAS && nota2 >= NOTA_MINIMA_MATERIAS) {

			if (total >= NOTA_MINIMA_PARA_PASSAR) {
				System.out.println("Aluno Aprovado");
			}
		} else {

			if (nota1 < NOTA_MINIMA_MATERIAS) {
				System.out.println("Voc� n�o alcan�ou nota suficiente em matem�tica que � 60 sua nota foi: " + nota1);
			}
			if (nota2 < NOTA_MINIMA_MATERIAS) {
				System.out.println("Voc� n�o alcan�ou nota suficiente em portug�s que � 60 sua nota foi: " + nota2);
			}
		}

		scanner.close();
	}
	
	/*  Solu��o algar/works
	 * Scanner scanner = new Scanner(System.in);
	 * 
	 * System.out.println("VERIFICANDO SE TEM NOTA SUFICIENTE PARA CONCURSO.");
	 * 
	 * System.out.print("Portugu�s: "); Double notaDePortugues =
	 * scanner.nextDouble();
	 * 
	 * System.out.print("Matem�tica: "); Double notaDeMatematica =
	 * scanner.nextDouble();
	 * 
	 * Boolean estaAcimaDoMinimoEmPortugues = notaDePortugues >= NOTA_MINIMA;
	 * 
	 * Boolean estaAcimaDoMinimoEmMatematica = notaDeMatematica >= NOTA_MINIMA;
	 * 
	 * Double notaTotal = notaDePortugues + notaDeMatematica;
	 * 
	 * Boolean temNotaParaPassar = notaTotal >= NOTA_DESCLASSIFICATORIA_GERAL;
	 * 
	 * Boolean passou = temNotaParaPassar && estaAcimaDoMinimoEmPortugues &&
	 * estaAcimaDoMinimoEmMatematica;
	 * 
	 * if (passou) { System.out.println("Parab�ns! Voc� passou."); } else {
	 * System.out.println("Infelizmente, n�o foi dessa vez."); }
	 * 
	 * scanner.close();
	 */
	
	

	
	
}
