
public class OperadoresRealacionais {

	public static void main(String[] args) {

		Boolean treMaiorQueDois = 3 > 2;
		System.out.println("3 > 2 ? : " + treMaiorQueDois);
		
		Boolean treMaiorQueDois1 = 3 < 2;
		System.out.println("3 < 2 ? : " + treMaiorQueDois1);
		
		Boolean treMaiorQueDois2 = 3 > 3;
		System.out.println("3 > 3 ? : " + treMaiorQueDois2);
		
		Boolean treMaiorQueDois3 = 3 >= 3;
		System.out.println("3 >= 3 ? : " + treMaiorQueDois3);
		
		Boolean treMaiorQueDois4 = 3 != 3;
		System.out.println("3 != 3 ? : " + treMaiorQueDois4);
		
		Boolean treMaiorQueDois5 = 3 == 3;
		System.out.println("3 == 3 ? : " + treMaiorQueDois5);
		
		Integer treMaiorQueDois6 = 3;
		// Forma completa treMaiorQueDois6 = treMaiorQueDois6 + 3;
		treMaiorQueDois6 += 3;
		System.out.println( treMaiorQueDois6);
		

	}

}
