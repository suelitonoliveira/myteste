import java.util.Locale;
import java.util.Scanner;

public class Exercicio01 {

	public static void main(String[] args) {
		// Exercicio de IMC
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
				
		System.out.print("Entre com seu peso: ");
		Double peso = sc.nextDouble();
		System.out.print("Entre com sua Altura: ");
		Double altura = sc.nextDouble();
		
		Double result = peso / (altura * altura);
		if (result >= 24.9 && result <= 30) {
			System.out.println("Voc� esta com SobrePeso ");
			System.out.printf("Seu IMC: %.2f%n", result);
			System.out.println("Sua morte est� pr�xima cuide do seu peso!");
		}else {
			
			System.out.println("Voc� esta com Peso Certo");
			System.out.printf("Seu IMC: %.2f%n ", result);
		}
		
		
		
		sc.close();
		
	}

}
