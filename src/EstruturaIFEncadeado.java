import java.util.Scanner;

public class EstruturaIFEncadeado {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Digite o peso: ");
		Double peso = sc.nextDouble();
		
		Boolean pesoLeve = peso <= 60;
		Boolean pesoMedio = (peso > 60) && (peso <= 90);
		Boolean pesoPesado = peso >90;
		
		if (pesoLeve) {
			System.out.println("O lutador(a) � peso leve.");
		}else {
			if (pesoMedio) {
				System.out.println("O lutador(a) � peso m�dio");
			}else {
				if (pesoPesado) {
					System.out.println("O lutador(a) � peso pesado.");
				}
			}
			
		}
		
		sc.close();
		

	}

}
