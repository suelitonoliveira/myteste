import java.util.Locale;
import java.util.Scanner;

public class Algoritimo1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
			Locale.setDefault(Locale.US); 
			
			System.out.print("digite o valor do produto: ");
			double valor = scanner.nextDouble();
			
			System.out.print("digite a quantidade do produto: ");
			double quantidade = scanner.nextDouble();
			
			double subtotal = valor * quantidade;
			
			boolean total = quantidade > 10 ;
			
			double desconto = 0.0;
			
			if (total) {
				desconto = 10.0;
				
			}
			
			double conta = subtotal * desconto / 100;
			double totalFinal  = subtotal - conta;
			
			System.out.printf("Valor a Paga e: %.2f%n" , totalFinal);		
		
		
		scanner.close();
		

	}

}
