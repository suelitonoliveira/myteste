import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
 
public class ReadWriteImage {   
    public static void main( String[] args ) {
        BufferedImage image = null;
        try {
           
            URL url = new URL("https://ibcdn.canaltech.com.br/I57q7__L1DbsBY-b47yZMXFPnjM=/filters:watermark(wm/v1.png,center,center,1,20)/i257633.jpeg");
            image = ImageIO.read(url);
             
            ImageIO.write(image, "jpg",new File("C:\\out.jpg"));
            ImageIO.write(image, "gif",new File("C:\\out.gif"));
            ImageIO.write(image, "png",new File("C:\\out.png"));
             
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Done");
    }
}