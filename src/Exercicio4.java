import java.util.Scanner;

public class Exercicio4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Nota : ");
		Double nota = sc.nextDouble();

		Boolean result = nota >= 70;

		if (result) {
			System.out.println("Voc� foi APROVADO! sua nota � : " + nota);
		} else {
			System.out.println("Voc� foi REPROVADO! sua nota � :  " + nota);
		}

		sc.close();

	}

}
