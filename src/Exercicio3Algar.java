import java.util.Scanner;

public class Exercicio3Algar {

	public static void main(String[] args) {

		/*
		 * forma feia Scanner sc = new Scanner(System.in);
		 * 
		 * System.out.println("Valor conta de luz: "); double luz = sc.nextDouble();
		 * 
		 * System.out.println("Valor conta de �gua: "); double agua = sc.nextDouble();
		 * 
		 * System.out.println("Valor conta de telefone: "); double telefone =
		 * sc.nextDouble();
		 * 
		 * System.out.println("Valor conta de escola: "); double escola =
		 * sc.nextDouble();
		 * 
		 * System.out.println("Valor conta de cart�o: "); double cartao =
		 * sc.nextDouble();
		 * 
		 * System.out.println("Valor conta de supermercado: "); double supermercado =
		 * sc.nextDouble();
		 * 
		 * double total = 0;
		 * 
		 * System.out.println(total+=luz+=agua+=telefone+=escola+=cartao+=supermercado);
		 * 
		 * 
		 * sc.close();
		 */

		Scanner scanner = new Scanner(System.in);

		System.out.println("Digite abaixo os valores gastos com...");

		Double total = 0.0;

		System.out.print("Luz: ");
		total += scanner.nextDouble();

		System.out.print("�gua: ");
		total += scanner.nextDouble();

		System.out.print("Telefone: ");
		total += scanner.nextDouble();

		System.out.print("Escola: ");
		total += scanner.nextDouble();

		System.out.print("Cart�o: ");
		total += scanner.nextDouble();

		System.out.print("Supermercado: ");
		total += scanner.nextDouble();

		System.out.println("O gasto total foi de: " + total);

		scanner.close();

		/*
		 * O valor da conta de luz Conta de �gua Conta de telefone Escola do filho
		 * Fatura do cart�o Gastos com supermercado ... e mostre o gasto total que a
		 * fam�lia teve no m�s. N�o esque�a de usar muito o operador "+=".
		 */
	}

}
